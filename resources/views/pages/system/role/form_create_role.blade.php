    <div class="card card-custom">
        <div class="card-header flex-wrap border-1 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Form Role
                </h3>
            </div>
            <div class="card-toolbar">
        
            </div>
        </div>
        <div class="card-body">
            <form action="javascript:;" method="post" id="form-create-role" enctype="multipart/form-data">
                <input type="hidden" name="role_id" id="role_id" value=""/>
              
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Role <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input name="role_ct" id="role_ct" type="text" class="form-control input-sm" value="">
                    </div>
                </div>
               
                <div class="text-right">
                    <a class="btn btn-outline-primary font-weight-bolder btn-back-roles">
                        <i class="fas fa-angle-double-left"></i> Kembali
                    </a>
                    <button type="reset"  class="btn btn-default">Reset <i class="flaticon2-reload"></i></button>
                    <button type="submit"  class="btn btn-primary btn-save-form-create-role">Simpan <i class="flaticon-paper-plane"></i></button>
                </div>
            </form>

         
        </div>
    </div>